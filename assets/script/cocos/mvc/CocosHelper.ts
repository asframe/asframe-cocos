import { BasicView } from "../../aframe/mvc/view/BasicView";
import ComponentScript from "./ComponentScript";
import { MC } from "../../aframe/mvc/MC";
import { Net } from "../../aframe/net/Net";
import { NoticeData } from "../../aframe/mode/observer/NoticeData";
import { BasicHelper } from "../../aframe/mvc/helper/BasicHelper";
import { IView } from "../../aframe/mvc/view/IView";

/**
 * 框架里自带的常量，供其他应用直接调用
 * @author sodaChen
 * Date:2018-9-20
 */
export class CocosHelper<V extends IView<any,any,any>,D> extends BasicHelper<V,D>
{
    /**
     * 存放cmd的监听指令，释放的时候自动删除事件
     */
    private cmdEvents:NoticeData[];

    constructor()
	{
        super();
        this.cmdEvents = [];
    }

    /**
     * 监听网络数据，会在close和destory的时候进行清除
     * @param notice 
     * @param listener 
     * @param thisObj 
     */
    cmdOn(notice,listener,thisObj)
    {
        this.cmdEvents.push(Net.cmds.on(notice, listener, thisObj));
    }
    destroy(o: any = null): void 
    {
        if (this.isDestroy)
             return;
        super.destroy(o);
        //释放所有的事件
        let length = this.cmdEvents.length;
        let noticeData:NoticeData = null;
        for (let index = 0; index < length; index++)
        {
            noticeData = this.cmdEvents[index];
            if(!noticeData)
                Net.cmds.off(noticeData.notice,noticeData.listener,noticeData.thisObj);
        }

    }

}