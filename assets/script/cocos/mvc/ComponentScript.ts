const {ccclass, property} = cc._decorator;

/**
 * 专门用于绑定组件的脚本代码,代理事件，用来发出相关的时间
 */
@ccclass
export default class ComponentScript extends cc.Component 
{
    /**
     * 组件脚本被调用 start方法之后会发出这个事件
     */
    static ComponentStart:string = "ComponentStart";

    constructor()
    {
        super();
        console.log("ComponentScript进行构造");
    }
    /**
     * 脚本开始了，发出事件
     */
    start () 
    // onLoad () 
    {
        console.log("ComponentScript组件完成初始化, nodeName：" + this.node.name);
        this.node.dispatchEvent(new cc.Event(ComponentScript.ComponentStart,false));
    }
}