import { ButtonMgr } from "../../ddz/mgr/ButtonMgr";

/**
 * 框架里自带的常量，供其他应用直接调用
 * @author sodaChen
 * Date:2018-9-20
 */
export class CCMvcUtils
{
    /**
     * 自动绑定节点上的属性
     * @param node 
     * @param view 
     */
    static bingNode(node:cc.Node,view:any):void
    {
        var childsLen:number = node.childrenCount;
        for(let i:number = 0; i < childsLen; i++)
        {
            var child:cc.Node = node.children[i];
            var name:string = child.name;
            if(name.indexOf("_node") != -1)
            {
                //console.log("有节点了");
                this.bingNode(child,view);
            }
            else if(name.indexOf("_code") != -1)
            {
                //console.log("自动代码");
                //其实这个还是节点，需要对元件进行处理
                //如果是按钮则进行自动注册事件，目前只有Label，有label就必定Label，否则就是其他节点
                this.bingNode(child,view);
                let comButton:cc.Component = child.getComponent(cc.Button);
                if (comButton) {
                    //child.getComponent(cc.Button).transition = cc.Button.Transition.NONE;
                    ButtonMgr.instance.buttonAction(child);
                    view.buttonList.push(child);
                }

                let com:cc.Component = child.getComponent(cc.Label);
                if(com)
                    view[name.split("_")[0]] = child.getComponent(cc.Label);
                else
                    view[name.split("_")[0]] = child;
            }
            else{
                this.bingNode(child,view);
                let comButton:cc.Component = child.getComponent(cc.Button);
                if (comButton) {
                    //child.getComponent(cc.Button).transition = cc.Button.Transition.NONE;
                    ButtonMgr.instance.buttonAction(child);
                    view.buttonList.push(child);
                }
            }
        }           
    }
}