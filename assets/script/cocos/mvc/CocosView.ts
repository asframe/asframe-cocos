import { BasicView } from "../../aframe/mvc/view/BasicView";
import ComponentScript from "./ComponentScript";
import { MC } from "../../aframe/mvc/MC";
import { Net } from "../../aframe/net/Net";
import { NoticeData } from "../../aframe/mode/observer/NoticeData";
import { ButtonMgr } from "../../ddz/mgr/ButtonMgr";
// import { ReyunUtils } from "../../ddz/utils/ReyunUtils";//TODO:循环引用DB 

/**
 * 框架里自带的常量，供其他应用直接调用
 * @author sodaChen
 * Date:2018-9-20
 */
export class CocosView<D,M> extends BasicView<cc.Node,D,M>
{
    /** 和Prefab绑定的脚本，通用脚本（以后根据需要来进行扩展） */
    protected mViewScript:ComponentScript;
    /**
     * 存放cmd的监听指令，释放的时候自动删除事件
     */
    private cmdEvents:NoticeData[];

    private buttonList: cc.Node[] = [];

    constructor()
	{
        super();
        this.cmdEvents = [];
    }

    init():void
    {
        // console.log("CocosView init......");
        console.log("热云统计 ===========> onInit: "+this.name);
        // ReyunUtils.reyunRestPagesEvent(this.name);
        //到这里的时候，框架已经加载好对应的Prefab资源了，而且也add到对应的node上面去
        //其中container对应的就是你加载进来的Prefab
        //绑定组件脚本
        this.mViewScript = this.container.addComponent(ComponentScript);
        //监听初始化完成方法
        this.container.once(ComponentScript.ComponentStart,this.onComponentStart,this);
    }

    protected onComponentStart(evt:cc.Event):void
    {
        console.log("监听到组件初始化完成事件");
        //回调模板方法，处理子类需要初始化的相关东西
        this.onInit();
        //抛出完成事件，通知框架底层做一些其他处理
        this.send(MC.VIEW_COMPLETE);
    }
    /**
     * 初始化方法，子类进行重写，初始化所有的view操作
     */
    onInit():void
    {

    }
    /**
     * 监听网络数据，会在close和destory的时候进行清除
     * @param notice 
     * @param listener 
     * @param thisObj 
     */
    cmdOn(notice,listener,thisObj)
    {
        this.cmdEvents.push(Net.cmds.on(notice, listener, thisObj));
    }
    destroy(o: any = null): void 
    {
        console.info("销毁view:" + this.name);
        if (this.isDestroy)
             return;
        super.destroy(o);
        //释放所有的事件
        let length = this.cmdEvents.length;
        let noticeData:NoticeData = null;
        for (let index = 0; index < length; index++)
        {
            noticeData = this.cmdEvents[index];
            if(noticeData)
                Net.cmds.off(noticeData.notice,noticeData.listener,noticeData.thisObj);
        }

        for (let BtnIndex = 0; BtnIndex < this.buttonList.length; BtnIndex++) {
            let btnElement = this.buttonList[BtnIndex];
            ButtonMgr.instance.stopAction(btnElement);
        }
        this.container.removeAllChildren(true);
        this.container.resumeAllActions();
        this.container.destroy();
        this.container = null;
        delete this.container;
    }
}