import { IResLoader } from "../../aframe/mvc/core/IResLoader";

import { IReser } from "../../aframe/mvc/core/IReser";
import { HashMap } from "../../aframe/maps/HashMap";
import { ResBean } from "../../aframe/mvc/bean/ResBean";
import { CCResMgr } from "../mgr/CCResMgr";

/**
 * @MvcResLoader.ts
 *
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:Collections
 * <br>Date:2017/4/11
 */
// namespace game
// {
    /**
     * mvc框架的资源加载器
     * @author sodaChen
     * Date:2017/4/11
     */
    export class MvcResLoader implements IResLoader
    {

        /**资源加载完成事件 */
        public static MvcResLoadComplete_Event: string = "MvcResLoadComplete_Event";
        private reser: IReser;
        private loadCount: number;
        private groupDic: Object;
        // private resList: mvc.ResBean[];
        // private groupName: string;
        // private static loadCompleteDic: asf.Dictionary<string, number>;
        private static loadCompleteDic: HashMap<string, number>;

        static loadingViewClass: any;
        /**当前的加载界面是否归属自己 */
        //      private myloadingViewClass: any;

        constructor()
        {
            if (MvcResLoader.loadCompleteDic == null)
            {
                MvcResLoader.loadCompleteDic = new HashMap<string, number>();
            }
        }
        destroy(o?: any): void
        {
            // RES.removeEventListener(RES.ResourceEvent.GROUP_COMPLETE, this.onResourceLoadComplete, this);
            // RES.removeEventListener(RES.ResourceEvent.GROUP_LOAD_ERROR, this.onResourceLoadError, this);
            // RES.removeEventListener(RES.ResourceEvent.GROUP_PROGRESS, this.onResourceProgress, this);
            // RES.removeEventListener(RES.ResourceEvent.ITEM_LOAD_ERROR, this.onItemLoadError, this);
            delete this.reser;
            //  this.myloadingViewClass = null;
        }

        /**
         * 删除资源
         * @param group
         */
        static removeRes(path: string): void
        {
            var count: number = this.loadCompleteDic.get(path);
            if (count)
            {
                count--;
            }

            if (!count || count == 0)
                this.loadCompleteDic.remove(path);
            else
                MvcResLoader.loadCompleteDic.put(path, count);
        }

        /**
         * 目前只是载入一个
         * @param resList
         * @param reser
         */
        loadResList(resList: ResBean[], reser: IReser): void
        {
            // this.resList = resList;
            this.reser = reser;
            //目前就只有一个
            let resBean:ResBean = resList[0];
            //先检查一下有没有缓存资源
            let res = CCResMgr.getCacheRes(resBean.path);
            if(res)
            {
                //开始实例化预制资源 
                var node:cc.Node = cc.instantiate(res);
                //资源回调过去
                reser.setResList([node],resList);
                return ;
            }
            //加载预制资源 
            var self = this;
            cc.loader.loadRes(resBean.path, function(errorMessage,loadedResource)
            { 
                //检查资源加载 
                // if( errorMessage) 
                // { 
                //     console.error( resBean.path + '载入预制资源失败, 原因:' + errorMessage ); 
                //     return; 
                // } 
                if( !( loadedResource instanceof cc.Prefab ) ) 
                { 
                    console.error( '你载入的不是预制资源!'  + resBean.path); 
                    return; 
                } 
                //开始实例化预制资源 
                var node:cc.Node = cc.instantiate(loadedResource);
                //资源回调过去
                reser.setResList([node],resList);
            });
        }
    }
// }