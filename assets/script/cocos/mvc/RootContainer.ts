import { HashMap } from "../../aframe/maps/HashMap";

import { MC } from "../../aframe/mvc/MC";

/**
 * @RootContainer.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/2/7
 */
// namespace mvc
// {
    /**
     * 跟容器对象，存放了游戏中各种基层容器对象
     */
    export class RootContainer
    {
        private static instance: RootContainer;
        /** 根容器对象，游戏引擎中的根对象 **/
        private rootContainer: cc.Node;
        /** 容器集合 **/
        private containerMap: HashMap<string, cc.Node>;

        stage: cc.Node;
        /** 背景层 **/
        bgLayer: cc.Node;
        /** 场景层 **/
        sceneLayer: cc.Node;
        /** bg界面最低层 **/
        viewBgLayer: cc.Node;
        /** 主UI层 **/
        mainLayer: cc.Node;
        /** 界面最低层 **/
        viewBottomLayer: cc.Node;
        /** 界面层 **/
        viewLayer: cc.Node;
        /** 控制层 **/
        controllerLayer: cc.Node;
        /** 界面顶层 **/
        viewTopLayer: cc.Node;
        /** 提示层 **/
        tipLayer: cc.Node;
        /** 最顶层 **/
        topLayer: cc.Node;

        public constructor(rootContainer: cc.Node)
        {
            this.containerMap = new HashMap<string, cc.Node>();
            this.rootContainer = rootContainer;
            //添加相关的层
            this.createContainer(MC.BG_LAYER);
            this.createContainer(MC.SCENE_LAYER);
            this.createContainer(MC.VIEW_BG_LAYER);
            this.createContainer(MC.MAIN_LAYER);
            this.createContainer(MC.VIEW_BOTTOM_LAYER);
            this.createContainer(MC.VIEW_LAYER);
            this.createContainer(MC.Controller_LAYER);
            this.createContainer(MC.VIEW_TOP_LAYER);
            this.createContainer(MC.TIP_LAYER);
            this.createContainer(MC.TOP_LAYER);

            //asf.App.stage.addEventListener(egret.Event.RESIZE, this.onResize, this);
            //this.onResize();
        }

        /*onResize(e?: egret.Event): void
        {
            var heightRatio: number = asf.App.stage.stageWidth / asf.App.stage.stageHeight
            if (heightRatio <= 1.6)
            {
                asf.App.stage.scaleMode = egret.StageScaleMode.FIXED_WIDTH
            }
            else
            {
                asf.App.stage.scaleMode = egret.StageScaleMode.FIXED_HEIGHT
            }
        }*/

        static getInstance(rootContainer?: cc.Node): RootContainer
        {
            if (!this.instance)
                this.instance = new RootContainer(rootContainer);
            return this.instance;
        }

        /**
         * 创建一个指定层名称的容器对象
         * @param layer
         */
        private createContainer(layer: string): void
        {
            var sprite: cc.Node = new cc.Node();
            this.rootContainer.addChild(sprite);
            this.containerMap.put(layer, sprite);
            this[layer] = sprite;
        }

        /**
         * 根据层的名称返回相应的容器对象
         * @param layer
         * @returns {cc.Node}
         */
        getContainer(layer: string): cc.Node
        {
            return this.containerMap.get(layer);
        }

        /**
         * 添加child到指定层中
         * @param layer
         * @param child
         */
        addChild(layer: string, child: cc.Node): void
        {
            this.containerMap.get(layer).addChild(child);
        }

        /**
         * 从指定层中删除child
         * @param layer
         * @param child
         */
        removeChild(layer: string, child: cc.Node): void
        {
            if (child.parent == this.containerMap.get(layer))
            {
                // console.log(layer + "层的容器成功删除的子对象的容器:" + child);
                this.containerMap.get(layer).removeChild(child);
            }
            else
            {
                // console.log(layer + "层的容器不是需要删除的子对象的容器:" + child);
                // console.log("child的父容器:" + child.parent);
            }
        }
    }
// }