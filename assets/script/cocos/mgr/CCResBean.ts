/**
 * @ResMgr.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ddzClient
 * <br>Date:2019/4/9
 */
/**
 * 资源管理器，实现并发下载和队列下载，默认是采用并发加载，会采用2线程进行进行加载。
 * 队列加载则会保证顺序的加载，只是同样会占用并发的线程
 */
export class CCResBean
{
    url:string;
    clazz:any;
    /**
     * 环境对象
     */
    thats:any[];
    /**
     * 单个回调函数
     */
    callBack:Function;
    /**
     * 多个回调函数
     */
    callBacks:Function[];
}