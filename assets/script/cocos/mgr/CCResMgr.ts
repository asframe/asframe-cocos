/**
 * @ResMgr.ts
 * @author sodaChen mail:asframe@qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ddzClient
 * <br>Date:2019/4/9
 */
import { HashMap } from "../../aframe/maps/HashMap";
import { CCResBean } from "./CCResBean";
import { CallBack } from "../../aframe/fun/CallBack";
/**
 * 资源管理器，实现并发下载和队列下载，默认是采用并发加载，会采用2线程进行进行加载。
 * 队列加载则会保证顺序的加载，只是同样会占用并发的线程
 * 
 * @author sodaChen
 * @date 2019年6月25日
 */
export class CCResMgr
{
    /**
     * 需要加载的资源集合
     */
    private static loadResMap:HashMap<string,CCResBean> = new HashMap();
    /**
     * 需要加载的资源顺序
     */
    private static queueRes:string[] = [];
    /**
     * 是否正在加载中
     */
    private static isLoading:boolean;
    /**
     * 需要预载资源的列表
     */
    private static priorResBeans:CCResBean[];
    /**
     * 预载资源加载完成的回调函数
     */
    private static priosCallBack:CallBack;
    /**
     * 缓存资源集合
     */
    private static cacheResMap:HashMap<string,any> = new HashMap();

    // /**
    //  * 获取到指定的资源
    //  * @param url 
    //  * @param callBack 
    //  */
    // public static getRes(url:string,callBack:Function)
    // {

    // }
    // /**
    //  * 
    //  * @param url 预先加载一组资源，并且会缓存起来
    //  * @param clazz 
    //  * @param callBack 
    //  */
    // public static priorLoadRes(urls:string[],clazz:any,callBack:Function):void
    // {
    //     var resListBean:CCResListBean = new CCResListBean();
    //     resListBean.urls = urls;
    //     resListBean.clazz = clazz;
    //     resListBean.callBack = callBack;
    // }
    /**
     * 添加一个预制资源
     * @param url 全路径
     * @param clazz 资源的类型
     */
    public static addPriorRes(url:string,clazz:any):void
    {
        if(!this.priorResBeans)
        {
            this.priorResBeans = [];
        }
        let resBean:CCResBean = new CCResBean();
        resBean.url = url;
        resBean.clazz = clazz;
        this.priorResBeans.push(resBean);
    }
    /**
     * 开始加载之前添加的预制资源.注意，加载预载资源的时候，正常的加载功能不可使用
     * （不支持同时加载预载资源又加载正常资源）
     * @param callBack 成功后的回调函数,函数参数是一个数字  1成功，0是失败
     */
    public static startLoadPriorRes(callBack?:Function,that?:any):void
    {
        if(callBack)
        {
            this.priosCallBack = new CallBack(callBack,that);
        }
            
        let resBean:CCResBean = this.priorResBeans.shift();
        this.loadPriorRes(resBean.url,resBean.clazz);
    }
    private static loadPriorRes(url:string,clazz:any):void
    {
        //直接进行加载(后面优化考虑不使用匿名函数)
        let that = this;
        cc.loader.loadRes(url,clazz,function(err,prefab)
        {
            if(prefab)
            
                that["cacheResMap"].put(url,prefab);
                
            let beans = that["priorResBeans"];
            if(beans.length == 0)
            {
                if(that["priosCallBack"])
                {
                    that["priosCallBack"].execute(1);
                }
            }
            else
            {
                let resBean:CCResBean = beans.shift();
                that["loadPriorRes"](resBean.url,resBean.clazz);
            }
        });
    }
    /**
     * 根据url获取到对应的缓存资源
     * @param url 
     */
    public static getCacheRes(url:string):any
    {
        return this.cacheResMap.get(url);
    }
    /**
     * 加载资源
     * @param url  加载资源路径 
     * @param callBack 回调函数
     */
    public static loadRes(url:string,clazz:any,callBack:Function,that:any):void
    {
        if(this.isLoading)
        {
            //放到队列缓存起来
            let resBean = this.loadResMap.get(url);
            if(resBean)
            {
                resBean.callBacks.push(callBack);
                resBean.thats.push(that);
            }
            else
            {
                resBean = new CCResBean();
                resBean.url = url;
                resBean.clazz = clazz;
                resBean.thats = [that];
                resBean.callBacks = [callBack];
                this.loadResMap.put(url,resBean);
                this.queueRes.push(url);
            }
        }
        else
        {
            this.isLoading = true;
            //直接进行加载(后面优化考虑不使用匿名函数)
            cc.loader.loadRes(url,clazz,function(err,prefab)
            {
                CCResMgr.loadResult(url,err,prefab);
            });
        }
    }

    private static loadResult(url:string,err:Error,prefab:any):void
    {
        //进行回调
        let resBean = this.loadResMap.remove(url);
        if(resBean)
        {
            for (let i = 0; i < resBean.callBacks.length; i++) 
            {
                try 
                {
                    resBean.callBacks[i].call(resBean.thats[i],err,prefab);
                }
                catch (error) 
                {
                    console.log(url + "进行函数回调报错");
                    console.error(error);
                }
            }
        }
        //检测队列是否还有新的url需要进行下载
        if(this.queueRes.length > 0)
        {
            //从第一个开始删除取出
            let url1 = this.queueRes.shift();
            resBean = this.loadResMap.get(url1);
            cc.loader.loadRes(url1,resBean.clazz,function(err1,prefab1)
            {
                CCResMgr.loadResult(url1,err1,prefab1);
            });
        }
        else
        {
            this.isLoading = false;
        }
    }
}