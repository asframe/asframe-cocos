/**
 * 框架里自带的常量，供其他应用直接调用
 * @author sodaChen
 * Date:2018-9-20
 */
export class BoostNode
{
    static boostNode():void
    {
        //扩展Node的方法，然后可以直接调用（还需要修改Node.d.ts）
        let prototype:any = cc.Node.prototype;
        prototype.getButton = function():cc.Button
        {
            return this.getComponent(cc.Button);
        }
        prototype.getButtonLabel = function():cc.Label
        {
            return this["getChildByName"]("Label")["getComponent"](cc.Label);
        }
        prototype.getLabel = function():cc.Label
        {
            return this.getComponent(cc.Label);
        }
    }
}