import { BoostNode } from "./BoostNode";

/**
 * 做一些扩展Cocos框架需要的扩展
 * @author sodaChen
 * Date:2018-9-20
 */
export class CocosMain
{
    static init():void
    {
        BoostNode.boostNode();
    }
}