import { InitModule } from "../mvc/core/InitModule";
import { JsonTabs } from "../../ddz/tables/JsonTabs";

/**
 * 音频管理
 * @author Peter
 * @date 2018-10-24
 *
 */
export class AudioUtils
{
    private static ins:AudioUtils = new AudioUtils().init();
    static get instance():AudioUtils
    {
        //console.log("获取音频管理实例");
        return this.ins;
    }

    bgmVolume:number = 1.0;
    sfxVolume:number = 1.0;
    bgmAudioID:number = -1;
    sfxAudioID:number = -1;
    
    init():AudioUtils
    {
        //console.log("AudioUtils init");
        let t = cc.sys.localStorage.getItem("bgmVolume");
        if(t != null){
            this.bgmVolume =parseFloat(t);
        }

        let s = cc.sys.localStorage.getItem("sfxVolume");
        if (s != null){
            this.sfxVolume = parseFloat(s);
        }

        cc.director.on(cc.game.EVENT_HIDE, function () {
            console.log("cc.audioEngine.pauseAll============");
            cc.audioEngine.pauseAll();
        });
        cc.director.on(cc.game.EVENT_SHOW, function () {
            console.log("cc.audioEngine.resumeAll===========");
            cc.audioEngine.resumeAll();
        });
        return this;
    }


    getUrl(url : string):string{
        return "sounds/" + url;
    }

    playBGM(url : string){
        let audioUrl = this.getUrl(url);
        //console.log("playBGM url = "+audioUrl);

        if(this.bgmAudioID >= 0){
            cc.audioEngine.stop(this.bgmAudioID);
        }
        let self = this;
		cc.loader.loadRes(audioUrl, cc.AudioClip, function (err, clip) {
            if (err) {
		        cc.error(err.message || err);
		        return;
		    }
            self.bgmAudioID = cc.audioEngine.play(clip,true,self.bgmVolume);
		});

    }
    
    playSFX(url : string){
        let audioUrl = this.getUrl(url);
        // console.log("playSFX url = "+audioUrl + "  sfxAudioID = "+this.sfxAudioID);
        // if(this.sfxAudioID >= 0){
        //     cc.audioEngine.stop(this.sfxAudioID);
        // }

        let self = this;

        if(this.sfxVolume > 0){
            cc.loader.loadRes(audioUrl, cc.AudioClip, function (err, clip) {
                if (err) {
                    cc.error(err.message || err);
                    return;
                }
                self.sfxAudioID = cc.audioEngine.play(clip,false,self.sfxVolume);    
            });
        }
    }
    
    setSFXVolume(v : number){
        if(this.sfxVolume != v){
            cc.sys.localStorage.setItem("sfxVolume",v);
            this.sfxVolume = v;
        }
    }
    
    setBGMVolume(v:number,force:boolean){
        if(this.bgmAudioID >= 0){
            if(v > 0){
                cc.audioEngine.resume(this.bgmAudioID);
            }
            else{
                cc.audioEngine.pause(this.bgmAudioID);
            }
            //cc.audioEngine.setVolume(this.bgmAudioID,this.bgmVolume);
        }
        if(this.bgmVolume != v || force){
            cc.sys.localStorage.setItem("bgmVolume",v);
            this.bgmVolume = v;
            cc.audioEngine.setVolume(this.bgmAudioID,v);
        }
    }
    
    pauseAll(){
        cc.audioEngine.pauseAll();
    }
    
    resumeAll(){
        cc.audioEngine.resumeAll();
    }
}