import { ByteArray } from "../bytes/ByteArray";

/**
 * @IMsgCoder.ts
 * @author sodaChen mail:asframe#qq.com
 * @version 1.0
 * <br> Copyright (c) 2012-present, asframe.com
 * <br>Program Name:ASFrameTS
 * <br>Date:2017/2/24
 */
// namespace game
// {
    /**
     * 消息的编码和解码接口
     * @author sodaChen
     * Date:2017/2/24
     */
    export interface IMsgCodec
    {
        /**
         * 编码
         * @param data
         */
        code(data:any):ByteArray;
        /**
         * 解码
         * @param bytes
         */
        decode(backCmd:number,bytes:ByteArray):any;
        /**
         * 本地二进制解析器解码。和网络需要判断字节长度的不一样
         * @param bytes
         */
        localDecode(backCmd:number,bytes:ByteArray):any;
    }
// }