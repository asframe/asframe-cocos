import { Subjects } from "../mode/observers/Subjects";

import { IHoldupView } from "./core/IHoldupView";

import { Context } from "./Context";

import { ViewBean } from "./bean/ViewBean";

import { IView } from "./view/IView";

import { IModule } from "./module/IModule";
import { IViewHandler } from "./core/IViewHandler";
import { NoticeData } from "../mode/observer/NoticeData";

/**
 * Created by sodaChen on 2017/3/14.
 */
// namespace mvc
// {

export class mvc
{
    static mvc_subjects: Subjects;

    static init(viewHandler: IViewHandler, holdView: IHoldupView, resLoaderClass: Function, moduleClass?: Function,db?:any): void
    {
        Context.$holdupView = holdView;
        Context.$viewHanlder = viewHandler;
        Context.$resLoaderClass = resLoaderClass;
        Context.$moduleMgrClass = moduleClass;
        Context.$db = db;
        Context.init();
        this.mvc_subjects = Context.subjects;
    }
    /**
     * 直接打开一个View对象，如果view没有初始化，则会进行初始化。同时会进行相关的依赖注入
     * @param view
     * @param callBack
     * @param thisObj
     * @param param
     */
    static openView(view: number | string | Function, param?: any, isEnforce?: boolean, tmpNoClose: boolean = false): void
    {
        // console.log("==============mvc.openView================"+view);
        // ReyunUtils.reyunRestClickEvent("clickEvent");//热云统计-按钮点击
        Context.$openView(view, param, isEnforce, tmpNoClose);
    }

    // /**
    //  * 简化打开view的方法
    //  * @param {number | string | Function} view
    //  * @param param
    //  * @param {boolean} isEnforce
    //  */
    // static open(view: number | string | Function, param?: any, isEnforce?: boolean): void
    // {
    //     Context.$openView(view, null, null, param, isEnforce);
    // }

    /**
     * 销毁界面
     * @param view 界面的标识
     */
    static destroyView(view: number | string | Function): void
    {
        this.closeView(view, true);
    }

    /**
     * 关闭指定等级的view
     * @param lv
     */
    static closeViewByLv(lv: number = -1): void
    {
        Context.$viewHanlder.closeViewByLv(lv);
    }

    /**
        * 关闭指定层级的view
        * @param lv
        */
    static closeViewByLaver(layer: string): void
    {
        Context.$viewHanlder.closeViewByLaver(layer);
    }

    /**先隐藏某些层级 */
    static hideViewByLaver(layer: string): void
    {
        Context.$viewHanlder.hideViewByLaver(layer);
    }

    /**把之前隐藏的某些层级显示 */
    static showViewByLaver(layer: string): void
    {
        Context.$viewHanlder.showViewByLaver(layer);
    }

    /**
     * 关闭View对象
     * @param view view 唯一ID、名字和构造函数之一
     * @param isDestroy 是否销毁界面，默认是false
     */
    static closeView(view: number | string | Function, isDestroy: boolean = false): void
    {
        var bean: ViewBean = Context.$getViewBean(view);
        if (!bean)
            return;
        // throw new Error(view + "mvc.closeView方法必须通过MvcReg进行注册");
        if (!bean.instance)
        {
            return;
        }
        if (isDestroy)
            bean.instance.destroy();
        else
            bean.instance.close();
    }

    /**
     * 获得一个view
     * 
     * @export
     * @param {(number | string | Function)} view
     * @param {*} [param]
     */
    static getView(view: number | string | Function, param?: any): IView<any,any,any>
    {
        return Context.$getView(view, param);
    }

    // /**
    //  * 清除外部对view实例的各种引用，这里只能由view对象本身调用
    //  * @param view
    //  */
    // static $clearViewRef(view:number | string | Function):void
    // {
    //     if(this.caller instanceof BasicView)
    //     {
    //         console.log("调用者是BasicView的子类");
    //     }
    //     Context.$getViewBean(view).instance = null;
    //     //模块清除对view的引用
    //  }
    /**
    * 根据模块名称或者class构造函数获取到模块的实例
    * @param module 名称或者class构造函数
    * @returns {IModule} 模块的实例
    */
    static getModule(module: string | Function): IModule<any>
    {
        return Context.$getModule(module);
    }
    /**
     * 发起一个通知事件
     * @param notice 通知名称
     * @param param 参数数组
     */
    static notice(notice: string | number, param?: Array<any>): void
    {
        // Context.subjects.notice(notice, param);
        this.mvc_subjects.notice(notice, param);
    }
    /**
     * 发送一个mvc框架的全局通知
     * @param notice 通知主题
     * @param args 相关参数
     */
    static send(notice: string | number, ...args): void
    {
        // Context.subjects.notice(notice, args);
        this.mvc_subjects.notice(notice, args);
    }
    /**
     * 添加一个观察者通知
     * @param notice 通知名称
     * @param listener 通知监听函数
     *
     */
    static on(notice: string | number, listener: Function, thisObj: Object): NoticeData
    {
        // return Context.subjects.on(notice, listener, thisObj);
        return this.mvc_subjects.on(notice, listener, thisObj);
    }
    /**
     * 监听主题，只监听一次
     * @param notice 通知
     * @param listener 监听函数
     * @param thisObj listener的类对象
     */
    static once(notice: string | number, listener: Function, thisObj: Object): NoticeData
    {
        // return Context.subjects.once(notice, listener, thisObj);
        return this.mvc_subjects.once(notice, listener, thisObj);
    }
    /**
     * 删除一个观察者通知
     * @param notice 通知名称
     * @param listener 删除指定监听函数
     *
     */
    static off(notice: string | number, listener: Function, thisObj: Object): void
    {
        // Context.subjects.off(notice, listener, thisObj);
        this.mvc_subjects.off(notice, listener, thisObj);
    }
    static offs(thisObj?: Object): void
    {
        // Context.subjects.offs(thisObj);
        this.mvc_subjects.offs(thisObj);
    }
    /**
     * 判断一个view是否处于打开状态
     * @param view 唯一ID、名字和构造函数之一
     */
    static isOpenView(view: number | string | Function):boolean
    {
        let viewBean = Context.$getViewBean(view);
        if(viewBean == null)
        {
            return false;
        }
        if(viewBean.instance == null)
            return false;
        return viewBean.instance.isOpen;
    }

    /**
     * 获取打开view实例
     * @param view 唯一ID、名字和构造函数之一
     */
    static getOpenViewBean(view: number | string | Function){
        let viewBean = Context.$getViewBean(view);
        if(viewBean == null)return null;
        return viewBean.instance;
    }



    /**
     * 是否打开了指定层级的view
     * @param layer:number 界面层级
     * @return boolean 是否打开
     */
    static hasViewByLayer(layer: string): boolean
    {
        return Context.$viewHanlder.hasViewByLayer(layer);
    }
}