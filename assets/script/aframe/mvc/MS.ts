import { Subjects } from "../mode/observers/Subjects";

/**
 * Created by soda on 2017/1/25.
 */
// namespace mvc
// {
    export class MS //extends asf.Singleton
    {
        /** 实例对象 **/
        private static instance:MS;

        /** mvc框架内通讯的全局主题对象 **/
        subjects:Subjects;

        constructor()
        {
            //super();
            this.subjects = new Subjects();
        }

        static getInstance():MS
        {
            if(!this.instance)
                this.instance = new MS();
            return this.instance;
        }
    }
// }