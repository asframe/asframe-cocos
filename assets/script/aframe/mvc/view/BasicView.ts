import { IView } from "./IView";

import { ViewConfig } from "../bean/ViewConfig";

import { IHelper } from "../helper/IHelper";

import { MC } from "../MC";
import { BasicCore } from "../core/BasicCore";
import { Context } from "../Context";
import { ArrayUtils } from "../../utils/ArrayUtils";
import { mvc } from "../mvc";
import { NoticeData } from "../../mode/observer/NoticeData";

/**
 * @SpriteView.as
 *
 * @author sodaChen mail:sujun10@qq.com
 * @version 1.0
 * <br>Program Name:DoEasy
 * <br>Date:2016-9-20
 */
// namespace mvc
// {
	/**
	 * mvc框架的基础View对象
	 * @author sodaChen
	 * #Date:2016-9-20
	 */
	export class BasicView<C,D,M> extends BasicCore<D> implements IView<C,D,M>
	{
		/**
		 * 界面级别,默认是0，也就是不做任何处理
		 */
		lv: number = 0;
		/**
		 * 界面是否打开
		 */
		isOpen: boolean;
		/**不关闭互斥界面 */
		noCloseIds: string[];

		/** 绑定Node节点上面的原件的名字列表，用于自动查找组件，并且进行自动绑定（这个列表手动编写或者用工具生成） */
		bingList:string[];
		/**
		 * 模块对象
		 */
		module:M;
		// /** 容器显示对象 **/
		container: C;
		/**
		 * 获取到View的配置数据，其中属性是和ViewConfig属性保持一直，
		 * 当需要重置ViewConfig的对应属性时，则在这object里增加对应的属性
		 * @return 包含相关属性的object对象
		 *
		 * @see com.asframe.doeasy.bean.ViewConfig,ModuleConfig,HelperConfig
		 */
		config: ViewConfig;
		/** 容器层名字， 默认是View容器层**/
		layer: string;
		/** 存放view的helper对象，用于统一管理 **/
		protected mHelpers: IHelper<IView<Object,Object,Object>,D>[];

		//		/** 是否已经初始化了 **/
		//		private var _isInit				:Boolean;
		// /** 是否已经关闭 **/
		// protected isClose: boolean;
		/** 自动释放，当关闭的时候就自动释放资源 **/
		protected isCloseDestroy: boolean = true;
		/**参数 */
		protected param: any;
		/**
		 * 存放cmd的监听指令，释放的时候自动删除事件
		 */
		private mvcEvents:NoticeData[];

		constructor()
		{
			super();
			this.mHelpers = [];
			this.layer = MC.VIEW_LAYER;
			this.lv = MC.VIEW_LV_1;
			//采用欺骗编译器的做法，主要是保留ViewConfig的打点提示，不要默认属性。
			//因为这个对象的属性会copy给ViewBean的config，所以不能实际使用ViewConfig
			let temp: any = {};
			this.config = temp;
			this.mvcEvents = [];
		}
		/**
		 * mvc的监听方法，使用这个方法，页面销毁时会自动释放相应的监听器
		 * @param notice 
		 * @param listener 
		 * @param thisObj 
		 */
		mvcOn(notice: string | number, listener: Function, thisObj: Object)
		{
			this.mvcEvents.push(mvc.on(notice,listener,thisObj));
		}
		/**
		 * 获取到获取容器的方法
		 * @deprecated
		 * @returns {C}
		 */
		getContainer(): C
		{
			return this.container;
		}
		addHelper(helper: IHelper<IView<Object,Object,Object>,D>): void
		{
			if (this.mHelpers.indexOf(helper) != -1)
				throw new Error("不能添加重复的helper");
			this.mHelpers.push(helper);
		}

		removeHelper(helper: IHelper<IView<Object,Object,Object>,D>): void
		{
			ArrayUtils.removeItem(this.mHelpers, helper);
		}

		start(): void
		{
			//			if(!_isInit)
			//				throw new Error(this + "View没有初始化或者没有调用initCompelete方法");
			//调用所有的helper的init
			//			for each (var helper:IHelper in mHelpers)
			//			{
			//				helper.init();
			//			}
			this.onStart();
		}
		onStart(): void
		{
		}

		serverHandler(): boolean
		{
			return false;
		}

		onServerHandler(): void
		{
			// this.toOpen(this.param);
		}

		open(param?: any): void
		{
			//添加一个view
			this.param = param;
			// if (!this.serverHandler())
			// {
			this.toOpen(param);
			// }
		}

		protected toOpen(param?: any): void
		{
			this.isOpen = true;
			//todo 因为cocose必须在添加到舞台之后才会触发绑定的脚本完成事件
			//检测一下，如果是之前执行了close，导致被删除舞台，那么这个时候需要加回去
			Context.$tryAddView(this);
			this.onOpen(param);
		}

		onOpen(param?: any): void
		{

		}

		close(): void
		{
			 console.log("关闭View:" + this);
			if (!this.isOpen)
				return;
			this.isOpen = false;
			// this.isClose = true;
			this.isOpen = false;
			this.onClose();
			Context.$closeView(this);
			//自动释放，并且还没释放
			if (this.isCloseDestroy && !this.isDestroy)
			{
				this.destroy();
			}
		}

		/**
		 * 发出界面初始化完成事件。由子类控制调用
		 */
		initCompelete(): void
		{
			// this.mContainer = mContainer;
			//发出完成初始化事件
			this.notice(MC.VIEW_COMPLETE);
		}

		onClose(): void
		{

		}

		destroy(o: any = null): void
		{
			if (this.isDestroy)
				return;
			// //没有关闭，先执行关闭方法
			// if(!this.isClose)
			// 	this.close();
			// console.log("--------------destroy---------------");
			super.destroy(o);

			//释放所有的事件
			let length = this.mvcEvents.length;
			let noticeData:NoticeData = null;
			for (let index = 0; index < length; index++)
			{
				noticeData = this.mvcEvents[index];
				if(noticeData)
					mvc.off(noticeData.notice,noticeData.listener,noticeData.thisObj);
			}

			// console.log("--------------this.send(MvcConst.VIEW_DESTORY, this);---------------");
			//发出销毁事件
			this.send(MC.VIEW_DESTORY, this);
			//释放自己的helper资源
			for (var i: number = 0; i < this.mHelpers.length; i++)
			{
				this.mHelpers[i].destroy(o);
			}
		}

		/**正在加载界面资源 */
		resLoading(): void
		{

		}
	}
// }