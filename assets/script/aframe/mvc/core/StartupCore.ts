import { Dictionary } from "../../maps/Dictionary";

import { ModuleBean } from "../bean/ModuleBean";

import { ViewBean } from "../bean/ViewBean";

import { CallBack } from "../../fun/CallBack";

import { InitModule } from "./InitModule";
import { ObjectUtils } from "../../utils/ObjectUtils";
import { InitView } from "./InitView";
import { Global } from "../../lang/Global";
import { Context } from "../Context";
import { StringUtils } from "../../utils/StringUtils";
import { IModel } from "../model/IModel";

/**
 * @Startup.as
 *
 * @author sodaChen mail:sujun10#21cn.com
 * @version 1.0
 * <br>Copyright (C), 2013 asFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:DoEasy
 * <br>Date:2016-5-29
 */
// namespace mvc
// {
	/**
	 * 启动框架的核心部分，只有刚开始启动框架时才会初始化
	 * @author sodaChen
	 * Date:2016-5-29
	 */
	export class StartupCore
	{
		moduleMap: Dictionary<string | Function, ModuleBean>;
		viewMap: Dictionary<number | string | Function, ViewBean>;
		complete: CallBack;
		isStartup: boolean;
		moduleCount: number;

		constructor(moduleMap: Dictionary<string | Function, ModuleBean>,
			viewMap: Dictionary<number | string | Function, ViewBean>,
			complete: CallBack)
		{
			this.moduleMap = moduleMap;
			this.viewMap = viewMap;
			this.complete = complete;
		}
		startup(): void
		{
			this.isStartup = true;
			this.moduleCount = 0;
			//遍历所有的模块，然后对需要立刻初始化的进行初始化处理
			// var module: IModule;
			var moduleBean: ModuleBean;
			var initModule: InitModule = null;
			var moduleAry: ModuleBean[] = this.moduleMap.values();
			var initDic: Dictionary<Object, Object> = new Dictionary();
			for (var i: number = 0; i < moduleAry.length; i++)
			{
				moduleBean = moduleAry[i];
				//默认都是lazy = false，没有初始化的，统一进行初始化
				if (!moduleBean.config.lazy && !moduleBean.instance)
				{
					//表示已经在初始化了，主要是防止一个module里的两个view都是需要立刻初始化的
					if (initDic.hasKey(moduleBean))
						continue;
					initDic.put(moduleBean, moduleBean);
					this.moduleCount++;
					initModule = new InitModule();

					initModule.initModule(moduleBean, new CallBack(this.onInitModule, this));
					// //temp
					if (ObjectUtils.strNoNull(moduleBean.instance, "name"))
					{
						if (!this.moduleMap.hasKey(moduleBean.instance.name))
							this.moduleMap.put(moduleBean.instance.name, moduleBean);
					}
				}
			}
			//遍历一边view，检查一下是否有db需要提前初始化
			var viewAry: ViewBean[] = this.viewMap.values();
			var viewBean:ViewBean = null;
			for (var j: number = 0; j < viewAry.length; j++)
			{
				viewBean = viewAry[j];
				//过滤掉module初始化过的model(module为空就可以了)
				if(Context.$db && viewBean.dbClass && !viewBean.moduleBean)
				{
					if(viewBean.selfDB)
						continue;
					
	
					//注入到DB管理器中
					let mName:string = viewBean.dbClass["NAME"];
					mName = StringUtils.uncapitalize(mName);
					let model:IModel = new viewBean.dbClass(); 
					model.init();
					Context.$db[mName] = model;
					viewBean.selfDB = model;
				}
			}
			//单独处理view需要初始化的情况
			this.isStartup = false;
			if (this.moduleCount == 0)
			{
				this.startupView();
			}
		}
		onInitModule(bean: ModuleBean): void
		{
			this.moduleCount--;
			if (!this.isStartup && this.moduleCount == 0)
			{
				this.startupView();
			}
		}
		// finishModule():void
		// {
		//
		// }
		startupView(): void
		{
			this.isStartup = true;
			this.moduleCount = 0;
			//遍历所有的view，然后对需要立刻初始化的进行初始化处理
			var initDic: Dictionary<Object, Object> = new Dictionary();
			//这里有个问题，只检测模块的lazy，但是有些view是设置lazy = false的，目前是无法处理，得优化处理
			var views: Array<ViewBean> = this.viewMap.values();
			var bean: ViewBean;
			for (var i: number = 0; i < views.length; i++)
			{
				bean = views[i];
				//延迟初始化
				if (bean.config.lazy)
					continue;

				if (bean.instance)
					continue;

				//已经初始化过的
				if (initDic.hasKey(bean))
					continue;
				initDic.put(bean, bean);
				this.moduleCount++;
				new InitView(false, bean, CallBack.create(this.onInitView, this, true))
			}
			//处理view需要初始化的情况
			//单独处理view需要初始化的情况
			this.isStartup = false;
			if (this.moduleCount == 0 && this.complete)
			{
				this.finish();
			}
			else
			{
				Global.addRef(this);
			}
		}
		onInitView(bean: Object): void
		{
			this.moduleCount--;
			if (!this.isStartup && this.moduleCount == 0)
			{
				this.finish();
			}
		}
		finish(): void
		{
			this.complete.execute();
			this.complete = null;
			//自我删除全局引用
			Global.removeRef(this);
		}

		reInitModule(): void
		{
			var moduleAry: ModuleBean[] = this.moduleMap.values();
			var initDic: Dictionary<Object, Object> = new Dictionary();
			for (var i: number = 0; i < moduleAry.length; i++)
			{
				let moduleBean: ModuleBean = moduleAry[i];
				if (moduleBean.instance)
				{
					//防止init两遍
					if (initDic.hasKey(moduleBean))
						continue;
					initDic.put(moduleBean, moduleBean);
					moduleBean.instance.init();
				}
			}
		}
	}
// }