/**
 * Created by soda on 2017/1/26.
 */
// namespace mvc
// {
    export class MC
    {
        /** 界面资源的路径，这个属性只会在VIEW对象这里进行检测，如果有，则加载这个资源 */
        static UI_RES:string = "UI_RES";
        /////////////mvc view、module、heper的独立事件，只有监听具体的对象才能接受到这些方法//////////
        /** 界面初始化完毕抛出的事件 **/
        static VIEW_COMPLETE: string = "viewComplete";
        /** view打开事件，参数是view的name或者id **/
        static VIEW_OPEN: string = "viewOpen";
        /** view事关闭件，参数是view的name或者id **/
        static VIEW_CLOSE: string = "viewClose";
        /** view销毁事件，参数是view的name或者id **/
        static VIEW_DESTORY: string = "viewDestory";

        ///////////////////////关于的view事件通知///////////////////
        /** 打开view事件 **/
        static OPEN_VIEW: string = "openView";
        /** 关闭view事件 **/
        static CLOSE_VIEW: string = "openView";
        /** 销毁view事件 **/
        static DESTORY_VIEW: string = "openView";


        /**
         * view的层
         * @type {string}
         */
        static BG_LAYER: string = "bgLayer";
        static SCENE_LAYER: string = "sceneLayer";
        static VIEW_BG_LAYER: string = "viewBgLayer";
        static MAIN_LAYER: string = "mainLayer";
        static VIEW_BOTTOM_LAYER: string = "viewBottomLayer";
        static VIEW_LAYER: string = "viewLayer";
        static Controller_LAYER: string = "controllerLayer";
        static VIEW_TOP_LAYER: string = "viewTopLayer";
        static TIP_LAYER: string = "tipLayer";
        static TOP_LAYER: string = "topLayer";

        /** 不受排斥的界面级别 **/
        static VIEW_LV: number = -1;
        /////////////View的级别。1级，2级，3级，-1是没有级别
        static VIEW_LV_MAX: number = 3;
        static VIEW_LV_0: number = 0;
        static VIEW_LV_1: number = 1;
        static VIEW_LV_2: number = 2;
        static VIEW_LV_3: number = 3;
    }
// }