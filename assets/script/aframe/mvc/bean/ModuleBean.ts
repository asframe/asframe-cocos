import { BasicBean } from "./BasicBean";

import { ModuleConfig } from "./ModuleConfig";

import { ViewBean } from "./ViewBean";
import { IModule } from "../module/IModule";
import { IModel } from "../model/IModel";

/**
 * Created by soda on 2017/1/27.
 */
// namespace mvc
// {
    export class ModuleBean extends BasicBean<ModuleConfig,IModule<any>>
    {
        /** 自身的数据模块实力 */
		selfDB:IModel;
        /** 数据对象 */
        dbClass:any;
        // /** 模块实例对象 **/
        // module:IModule;
        viewBeans:ViewBean[];
        constructor(clazz:any,config?:ModuleConfig)
        {
            super(clazz,config);
            this.viewBeans = [];
        }
    }
// }