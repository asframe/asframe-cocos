import { TimeMgr } from "./mgr/TimeMgr";

/**
 * @Constants.as
 *
 * @author sodaChen mail:asframe#163.com
 * @version 1.0
 * <br>Copyright (C), 2012 ASFrame.com
 * <br>This program is protected by copyright laws.
 * <br>Program Name:ASFrame
 * <br>Date:2012-6-20
 */
/**
 * 框架里自带的常量，供其他应用直接调用
 * @author sodaChen
 * Date:2012-6-20
 */
export class asf
{
	/**
	 * 应用的开始时间
	 */
	private static $Start_Time:number = Date.now();
	/**
	 * 时间管理对象
	 */
	static timer:TimeMgr = new TimeMgr();
	/**
     * 用于计算相对时间。此方法返回自启动 Egret 框架以来经过的毫秒数。
     * @returns 启动 Egret 框架以来经过的毫秒数。
     * @version Egret 2.4
     * @platform Web,Native
     * @includeExample egret/utils/getTimer.ts
     * @language zh_CN
     */
    static getTimer() {
        return Date.now() - this.$Start_Time;
    }
    /**
     * 创建一个any对象（object对象）
     */
    static createAny(): any
    {
        return {};
    }
}
